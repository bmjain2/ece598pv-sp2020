use serde::{Serialize, Deserialize};
use crate::crypto::hash::{H256, Hashable, generate_random_H256};
use std::time::{SystemTime, UNIX_EPOCH};
use crate::crypto::merkle::MerkleTree;
use crate::transaction::{SignedTransaction};
use rand::Rng;
use hex_literal::hex;

impl Hashable for Header {
    fn hash(&self) -> H256 {
        let encoded = bincode::serialize(&self).unwrap();
        ring::digest::digest(&ring::digest::SHA256, &encoded).into()
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, Copy)]
pub struct Header {
    pub parent: H256,
    pub nonce: u32,
    pub difficulty: H256,
    pub timestamp: u128,
    pub merkle_root: H256,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Content {
    pub data: Vec<SignedTransaction>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Block {
    pub header: Header,
    pub content: Content,
}

impl Hashable for Block {
    fn hash(&self) -> H256 {
        self.header.hash()
    }
}

pub fn get_difficulty() -> H256 {
    (hex!("0000ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff")).into()
}

pub fn generate_genesis_block() -> Block {
    let data = Vec::<SignedTransaction>::new();
    let content = Content {
        data: data.clone(),
    };
    let merkle_tree = MerkleTree::new(&data);
    let root = merkle_tree.root();

    let parent = (hex!("0000000000000000000000000000000000000000000000000000000000000000")).into();

    let header = Header {
        parent: parent,
        nonce: 0,
        difficulty: get_difficulty(),
        timestamp: 0,
        merkle_root: root,
    };
    Block {
        header: header,
        content: content
    }
}

#[cfg(any(test, test_utilities))]
pub mod test {
    use super::*;
    use crate::crypto::hash::H256;

}
