use serde::{Serialize,Deserialize};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, UnparsedPublicKey, ED25519};
use rand::Rng;
use crate::crypto::hash::{H256, H160, Hashable};
use crate::crypto::key_pair;


#[derive(Serialize, Deserialize, Default, Clone)]
pub struct Transaction {
    pub sender_addr: H160,
    pub recv_addr: H160,
    pub value: f64,
    pub nonce: u32,
}

impl std::fmt::Debug for Transaction {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "sender: {:?} recv: {:?} nonce: {}, value: {}", self.sender_addr, self.recv_addr, self.nonce, self.value)
    }
}

impl Hashable for Transaction {
    fn hash(&self) -> H256 {
        let encoded = bincode::serialize(&self).unwrap();
        ring::digest::digest(&ring::digest::SHA256, &encoded).into()
    }
}

#[derive(Serialize, Deserialize, Debug, Default, Clone)]
pub struct SignedTransaction {
    pub transaction: Transaction,
    pub sign_bytes: Vec<u8>,
    pub pk_bytes: Vec<u8>,
}

impl Hashable for SignedTransaction {
    fn hash(&self) -> H256 {
        let encoded = bincode::serialize(&self).unwrap();
        ring::digest::digest(&ring::digest::SHA256, &encoded).into()
    }
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    let encoded = bincode::serialize(t).unwrap();
    key.sign(&encoded)
}

pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
    let public_key = UnparsedPublicKey::new(&ED25519, public_key);
    let encoded = bincode::serialize(t).unwrap();
    public_key.verify(&encoded, signature.as_ref()).is_ok()
}

/// Transaction Signature Check
pub fn verify_signed_trans(sign_trans: SignedTransaction) -> bool {
    // check if owner's address matches public key
    let from_addr = sign_trans.transaction.sender_addr;
    let pk_hash = ring::digest::digest(&ring::digest::SHA256, &sign_trans.pk_bytes);
    let pk_hash_bytes = pk_hash.as_ref();
    let mut raw_address: [u8; 20] = [0; 20];
    raw_address.copy_from_slice(pk_hash_bytes.get(12..).unwrap());
    let expected_address: H160 = raw_address.into();
    if (expected_address != from_addr) {
        println!("[verify_signed_trans] Owner address did not match public key");
        return false;
    }

    // verify signature using public key
    let encoded_trans = bincode::serialize(&sign_trans.transaction).unwrap();
    let public_key = UnparsedPublicKey::new(&ED25519, sign_trans.pk_bytes);
    public_key.verify(&encoded_trans, &sign_trans.sign_bytes).is_ok()
}


#[cfg(any(test, test_utilities))]
pub mod tests {
    use super::*;
    use crate::crypto::key_pair;

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random();
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }

    #[test]
    fn sign_verify_ne() {
        let t1 = generate_random_transaction();
        let t2 = generate_random_transaction();

        let key = key_pair::random();
        let signature = sign(&t1, &key);
        assert!(!verify(&t2, &(key.public_key()), &signature));
    }
}
