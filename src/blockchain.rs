use crate::block::{Block, generate_genesis_block};
use crate::crypto::hash::{H256, Hashable, H160};
use std::collections::HashMap;
use crate::crypto::hash::generate_random_H256;
use hex_literal::hex;
use log::info;
use crate::transaction::{verify_signed_trans};
use crate::state::{State, AccountData};
use crate::mempool::{Mempool};
use std::sync::{Arc, Mutex};


pub struct MetaBlock {
    pub block: Block,
    pub height: u32,
}

pub struct Blockchain {
    hashmap: HashMap<H256, MetaBlock>,
    ledger: HashMap<H256, State>,
    orphan_buffermap: HashMap<H256, Block>,     // parent hash -> orphan block
    depth: u32,
    tip: H256,
    arc_mempool: Arc<Mutex<Mempool>>,
}

pub enum InsertStatus {
    Invalid,
    Orphan,
    Valid(Vec<H256>),
}

impl Blockchain {
    /// Create a new blockchain, only containing the genesis block
    pub fn new(arc_mempool: &Arc<Mutex<Mempool>>) -> Self {
        let mut hashmap = HashMap::new();
        // [imp!] genesis block should be deterministic (same for all nodes)
        let genesis_block = generate_genesis_block();
        let metablock = MetaBlock {
            block: genesis_block.clone(),
            height: 1,
        };
        hashmap.insert(genesis_block.hash(), metablock);

        let mut ledger = HashMap::new();
        let mut orphan_buffermap = HashMap::new();

        Blockchain{
            hashmap: hashmap,
            ledger: ledger,
            orphan_buffermap: orphan_buffermap,
            depth: 1,
            tip: genesis_block.hash(),
            arc_mempool: Arc::clone(arc_mempool),
        }
    }

    pub fn populate_ico_state(&mut self, peer_addresses: Vec<H160>) {
        // Initial coin offering
        let mut state = State::new();
        for address in peer_addresses {
            let details = AccountData {
                nonce: 0,
                balance: f64::from(100),
            };
            state.insert(address, details);
        }
        self.ledger.insert(self.tip, state);
    }

    /// Insert a block into blockchain
    pub fn insert(&mut self, block: &Block) -> InsertStatus {
        if !(block.hash() <= block.header.difficulty) {
            info!("[blockchain.insert] Invalid block: block hash does not satisfy difficulty");
            return InsertStatus::Invalid;
        }

        // verify all transactions -- if invalid remove them from mempool
        let mut mempool = self.arc_mempool.lock().unwrap();
        let mut all_trans_valid = true;
        let signed_trans_list = block.content.data.clone();
        for signed_trans in signed_trans_list {
            if (!verify_signed_trans(signed_trans.clone())) {
                all_trans_valid = false;
                mempool.remove(signed_trans.hash());
            }
        }
        drop(mempool);

        if (!all_trans_valid) {
            println!("[blockchain.insert] Invalid block: transaction signature verification failed");
            return InsertStatus::Invalid;
        }

        if !(self.hashmap.contains_key(&block.header.parent)) {
            // Note: multiple orphan blocks cannot point to the same parent; the latest one will overwrite
            self.orphan_buffermap.insert(block.header.parent, block.clone());
            info!("[blockchain.insert] Orphan block handling");
            return InsertStatus::Orphan;
        }

        let parent_meta_block = self.hashmap.get(&block.header.parent).unwrap();
        if !(block.header.difficulty == parent_meta_block.block.header.difficulty) {
            info!("[blockchain.insert] Invalid block: difficulty does not match parent difficulty");
            return InsertStatus::Invalid;
        }

        let result = self.validate_transactions(&block);
        let mut new_block_hashes: Vec<H256> = Vec::new();

        if let None = result {
            println!("[blockchain.insert] Transactions incorrect wrt parent state");
            return InsertStatus::Invalid;
        } else {
            let new_state = result.unwrap();
            self.insert_helper(&block, new_state);
            new_block_hashes.push(block.hash());
        }

        // Orphan block handler
        let mut parent_block_hash = block.hash();
        while (self.orphan_buffermap.contains_key(&parent_block_hash)) {
            let child_block = self.orphan_buffermap.remove(&parent_block_hash).unwrap();

            let child_result = self.validate_transactions(&child_block);

            if let None = child_result {
                println!("[blockchain.insert] orphan block handler invalid transactions");
                break;
            } else {
                let new_child_state = child_result.unwrap();
                self.insert_helper(&child_block, new_child_state);
                new_block_hashes.push(child_block.hash());
                info!("[blockchain.insert] Orphan block handled");
                parent_block_hash = child_block.hash();
            }
        }

        return InsertStatus::Valid(new_block_hashes);
    }

    /// Transaction Spending Check
    fn validate_transactions(&mut self, block: &Block) -> Option<State> {
        // check if transactions are valid wrt parent state
        let mut all_trans_pass = true;
        let parent_state = self.ledger.get(&block.header.parent).unwrap().clone();
        let mut curr_state = parent_state.clone();
        let signed_trans_list = block.content.data.clone();

        let mut mempool = self.arc_mempool.lock().unwrap();

        for signed_trans in signed_trans_list {
            let trans = signed_trans.transaction.clone();
            let mut sender_acc = curr_state.get_account_data(trans.sender_addr);
            if !(trans.value > 0.0 && sender_acc.balance > 0.0 && (trans.value <= sender_acc.balance)) {
                all_trans_pass = false;
                mempool.remove(signed_trans.hash());
            }
            if (trans.nonce != sender_acc.nonce + 1) {
                all_trans_pass = false;
                mempool.remove(signed_trans.hash());
            }
            sender_acc.balance -= trans.value;
            sender_acc.nonce += 1;

            let mut recv_acc = curr_state.get_account_data(trans.recv_addr);
            recv_acc.balance += trans.value;

            curr_state.insert(trans.sender_addr, sender_acc);
            curr_state.insert(trans.recv_addr, recv_acc);
        }
        drop(mempool);

        if !(all_trans_pass) {
            None
        } else {
            Some(curr_state)
        }
    }

    fn insert_helper(&mut self, block: &Block, block_state: State) {

        if !(block.header.parent == self.tip()) {
            info!("[blockchain.insert] New fork in the blockchain");
        }
        let parent_meta_block = self.hashmap.get(&block.header.parent).unwrap();
        let parent_height = parent_meta_block.height;
        let curr_height = parent_height + 1;
        if curr_height > self.depth {
            self.tip = block.hash();
            self.depth = curr_height;
        }
        let metablock = MetaBlock {
            block: block.clone(),
            height: curr_height,
        };
        self.hashmap.insert(block.hash(), metablock);

        // print old state, transactions and new state
        let old_state = self.ledger.get(&block.header.parent).unwrap();
        println!("Processing block hash = {:?}", block.hash());
        println!("Old state:");
        println!("{:?}", old_state);
        for trans in block.content.data.clone() {
            println!("{:?}", trans.transaction);
        }
        println!("New state:");
        println!("{:?}", block_state);

        self.ledger.insert(block.hash(), block_state);

        if self.tip == block.hash() {
            // added block to longest chain; remove transactions from mempool
            let mut mempool = self.arc_mempool.lock().unwrap();

            let signed_trans_list = block.content.data.clone();
            for signed_trans in signed_trans_list {
                mempool.remove(signed_trans.hash());
            }

            drop(mempool);
        }
    }

    pub fn get_longest_chain_state(&self) -> State {
        self.ledger.get(&self.tip).unwrap().clone()
    }

    /// Get the last block's hash of the longest chain
    pub fn tip(&self) -> H256 {
        self.tip
    }

    pub fn depth(&self) -> u32 {
        self.depth
    }

    pub fn get_difficulty(&self, block_hash: H256) -> Option<H256> {
        if (self.hashmap.contains_key(&block_hash)) {
            let block_header = self.hashmap.get(&block_hash).unwrap().block.header;
            return Some(block_header.difficulty);
        } else {
            None
        }
    }

    pub fn get_total_blocks(&self) -> usize {
        self.hashmap.len()
    }

    pub fn contains_block_hash(&self, block_hash: H256) -> bool {
        self.hashmap.contains_key(&block_hash)
    }
    pub fn get_block(&self, block_hash: H256) -> Block {
        self.hashmap.get(&block_hash).unwrap().block.clone()
    }

    /// Get the last block's hash of the longest chain
    pub fn all_blocks_in_longest_chain(&self) -> Vec<H256> {
        let mut consensus_chain = Vec::<H256>::new();
        let mut curr_key = self.tip();
        while (self.hashmap.contains_key(&curr_key)) {
            consensus_chain.push(curr_key);
            curr_key = self.hashmap.get(&curr_key).unwrap().block.header.parent;
        }
        consensus_chain.reverse();
        consensus_chain
    }
}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::block::test::generate_random_block;
    use crate::crypto::hash::Hashable;

    #[test]
    fn insert_one() {
        let mut blockchain = Blockchain::new();
        let genesis_hash = blockchain.tip();
        let block = generate_random_block(&genesis_hash);
        blockchain.insert(&block);
        assert_eq!(blockchain.tip(), block.hash());

    }

    #[test]
    fn insert_many() {
        let mut blockchain = Blockchain::new();
        let mut tip = blockchain.tip();
        let mut block = generate_random_block(&tip);
        for i in 0..10 {
            tip = blockchain.tip();
            block = generate_random_block(&tip);
            blockchain.insert(&block);
        }
        assert_eq!(blockchain.tip(), block.hash());
    }

    #[test]
    fn insert_fork() {
        let mut blockchain = Blockchain::new();
        let genesis_hash = blockchain.tip();
        let block = generate_random_block(&genesis_hash);
        blockchain.insert(&block);
        assert_eq!(blockchain.tip(), block.hash());
        //Test1
        let block_2 = generate_random_block(&block.hash());
        blockchain.insert(&block_2);
        assert_eq!(blockchain.tip(), block_2.hash());
        //Test2
        let block_3 = generate_random_block(&block_2.hash());
        blockchain.insert(&block_3);
        let block_4 = generate_random_block(&block_2.hash());
        blockchain.insert(&block_4);
        assert_eq!(blockchain.tip(), block_3.hash());
        //Test3
        let block_5 = generate_random_block(&block_4.hash());
        blockchain.insert(&block_5);
        assert_eq!(blockchain.tip(), block_5.hash());
    }

    #[test]
    fn test_longest_blockchain() {
        let mut blockchain = Blockchain::new();
        let mut all_blocks_hash = Vec::<H256>::new();

        for i in 0..10 {
            let mut tip = blockchain.tip();
            all_blocks_hash.push(tip);
            let block = generate_random_block(&tip);
            blockchain.insert(&block);
        }
        all_blocks_hash.push(blockchain.tip());
        let longest_chain = blockchain.all_blocks_in_longest_chain();
        assert_eq!(longest_chain, all_blocks_hash);
    }

    #[test]
    fn test_fork_longest_blockchain() {

        let mut longest_chain = Vec::<H256>::new();
        let mut blockchain = Blockchain::new();
        let genesis_hash = blockchain.tip();
        longest_chain.push(genesis_hash);
        assert_eq!(blockchain.all_blocks_in_longest_chain(), longest_chain);

        let block = generate_random_block(&genesis_hash);
        blockchain.insert(&block);
        longest_chain.push(block.hash());
        assert_eq!(blockchain.tip(), block.hash());
        assert_eq!(blockchain.all_blocks_in_longest_chain(), longest_chain);

        let block_2 = generate_random_block(&block.hash());
        blockchain.insert(&block_2);
        longest_chain.push(block_2.hash());
        assert_eq!(blockchain.tip(), block_2.hash());
        assert_eq!(blockchain.all_blocks_in_longest_chain(), longest_chain);

        let block_3 = generate_random_block(&block_2.hash());
        blockchain.insert(&block_3);
        longest_chain.push(block_3.hash());
        assert_eq!(blockchain.tip(), block_3.hash());
        assert_eq!(blockchain.all_blocks_in_longest_chain(), longest_chain);

        let block_4 = generate_random_block(&block_2.hash());
        blockchain.insert(&block_4);
        assert_eq!(blockchain.tip(), block_3.hash());
        assert_eq!(blockchain.all_blocks_in_longest_chain(), longest_chain);

        let block_5 = generate_random_block(&block_4.hash());
        blockchain.insert(&block_5);
        longest_chain.pop();
        longest_chain.push(block_4.hash());
        longest_chain.push(block_5.hash());

        assert_eq!(blockchain.tip(), block_5.hash());
        assert_eq!(blockchain.all_blocks_in_longest_chain(), longest_chain);

    }

}
