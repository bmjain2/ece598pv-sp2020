use std::collections::{HashSet};
use crate::crypto::hash::{H160};
use rand::seq::SliceRandom;

/// contains all addresses - moi address + peer address
pub struct Directory {
    peer_address: HashSet<H160>,
    vec_address: Vec<H160>,
}

impl Directory {
    pub fn new() -> Self {
        Directory {
            peer_address: HashSet::new(),
            vec_address: Vec::new(),
        }
    }

    pub fn insert(&mut self, address: H160) -> bool {
        let is_new = self.peer_address.insert(address);
        if is_new {
            self.vec_address.push(address);
        }
        is_new
    }

    pub fn get_rand_address(&self) -> H160 {
        *self.vec_address.choose(&mut rand::thread_rng()).unwrap()
    }

    pub fn is_empty(&self) -> bool {
        self.peer_address.is_empty()
    }

    pub fn get_all_addresses(&self) -> Vec<H160> {
        self.vec_address.clone()
    }

}