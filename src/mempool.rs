use std::collections::{HashSet, HashMap, VecDeque};
use crate::transaction::{SignedTransaction};
use crate::crypto::hash::{H256, Hashable};

pub struct Mempool {
    transaction_hashes: HashSet<H256>,
    unprocessed_trans: HashMap<H256, SignedTransaction>,
    ordered_trans: VecDeque<H256>
}

impl Mempool {
    pub fn new() -> Self {
        let mut transaction_hashes = HashSet::new();
        let mut unprocessed_trans = HashMap::new();
        let mut ordered_trans = VecDeque::new();
        Mempool {
            transaction_hashes,
            unprocessed_trans,
            ordered_trans,
        }
    }

    pub fn insert(&mut self, transaction: SignedTransaction) {
        let trans_hash = transaction.clone().hash();
        if (!self.has_received(trans_hash)) {
            self.transaction_hashes.insert(trans_hash);
            self.ordered_trans.push_back(trans_hash);
            self.unprocessed_trans.insert(trans_hash, transaction);
        }
        
    }

    pub fn is_empty(&self) -> bool {
        self.unprocessed_trans.is_empty()
    }

    pub fn remove(&mut self, trans_hash: H256) {
        if (self.unprocessed_trans.contains_key(&trans_hash)) {
            self.unprocessed_trans.remove(&trans_hash);
            let index = self.ordered_trans.iter().position(|x| *x == trans_hash).unwrap();
            self.ordered_trans.remove(index);   
        } else {
            println!("Trying to remove a non-existing transaction");
        }
    }

    pub fn get_transactions(&mut self, limit: u32) -> Vec<SignedTransaction> {
        let mut new_transactions: Vec<SignedTransaction> = Vec::new();
        let mut count = 0;
        let mut trans_hash_iter = self.ordered_trans.iter().peekable();
        while (count < limit) {
            if let None = trans_hash_iter.peek() {
                break;
            }
            let trans_hash = trans_hash_iter.next().unwrap();
            let trans_result = self.unprocessed_trans.get(&trans_hash);
            if let None = trans_result {
                println!("VecDeque trying to access a transaction which does not exist in HashMap");
            } else {
                new_transactions.push(trans_result.unwrap().clone());
                count += 1;
            }
            
        }
        new_transactions
    }

    pub fn has_received(&self, trans_hash: H256) -> bool {
        self.transaction_hashes.contains(&trans_hash)
    }

    pub fn has_processed(&self, trans_hash: H256) -> bool {
        !self.unprocessed_trans.contains_key(&trans_hash)
    }

    pub fn get_transaction(&self, trans_hash: H256) -> SignedTransaction {
        self.unprocessed_trans.get(&trans_hash).unwrap().clone()
    }
}
