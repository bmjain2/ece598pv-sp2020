use super::message::Message;
use super::peer;
use crate::network::server::Handle as ServerHandle;
use crossbeam::channel;
use log::{debug, warn, info};

use std::sync::{Arc, Mutex};
use crate::blockchain::{Blockchain, InsertStatus};
use crate::crypto::hash::{H256, Hashable};
use crate::block::{Block};
use crate::mempool::{Mempool};
use crate::transaction::{SignedTransaction, verify_signed_trans};
use std::time::{SystemTime, UNIX_EPOCH};
use std::thread;
use crate::directory::{Directory};

#[derive(Clone)]
pub struct Context {
    msg_chan: channel::Receiver<(Vec<u8>, peer::Handle)>,
    num_worker: usize,
    server: ServerHandle,
    arc_blockchain: Arc<Mutex<Blockchain>>,
    arc_mempool: Arc<Mutex<Mempool>>,
    arc_directory: Arc<Mutex<Directory>>,
}

pub fn new(
    num_worker: usize,
    msg_src: channel::Receiver<(Vec<u8>, peer::Handle)>,
    server: &ServerHandle,
    arc_blockchain: &Arc<Mutex<Blockchain>>,
    arc_mempool: &Arc<Mutex<Mempool>>,
    arc_directory: &Arc<Mutex<Directory>>,
) -> Context {
    Context {
        msg_chan: msg_src,
        num_worker,
        server: server.clone(),
        arc_blockchain: Arc::clone(arc_blockchain),
        arc_mempool: Arc::clone(arc_mempool),
        arc_directory: Arc::clone(arc_directory),
    }
}

impl Context {
    pub fn start(self) {
        let num_worker = self.num_worker;
        for i in 0..num_worker {
            let cloned = self.clone();
            thread::spawn(move || {
                cloned.worker_loop(i);
                warn!("Worker thread {} exited", i);
            });
        }
    }

    fn worker_loop(&self, worker_id: usize) {
        let mut total_block_delay = 0;
        let mut total_block_count = 0;
        loop {
            let msg = self.msg_chan.recv().unwrap();
            let (msg, peer) = msg;
            let msg: Message = bincode::deserialize(&msg).unwrap();

            match msg {
                Message::Ping(nonce) => {
                    debug!("Ping: {}", nonce);
                    peer.write(Message::Pong(nonce.to_string()));
                }
                Message::Pong(nonce) => {
                    debug!("Pong: {}", nonce);
                }

                Message::NewBlockHashes(block_hashes) => {
                    /*
                        Iterate through incoming block hash vector,
                        check which block hashes are missing from blockchain and
                        request those blocks from peer using GetBlocks message.
                    */
                    let mut new_block_hashes: Vec<H256> = Vec::new();

                    let mut blockchain = self.arc_blockchain.lock().unwrap();

                    for block_hash in block_hashes {
                        if !blockchain.contains_block_hash(block_hash) {
                            new_block_hashes.push(block_hash);
                        }
                    }
                    if new_block_hashes.len() > 0 {
                        peer.write(Message::GetBlocks(new_block_hashes));
                    }
                }

                Message::GetBlocks(block_hashes) => {
                    /*
                        Iterate through incoming block hashes requested by peer,
                        check which block hash is present in the blockchain and
                        send those blocks to the requesting peer using Blocks message.
                    */
                    let mut new_blocks: Vec<Block> = Vec::new();
                    
                    let mut blockchain = self.arc_blockchain.lock().unwrap();
                    
                    for block_hash in block_hashes {
                        if blockchain.contains_block_hash(block_hash) {
                            new_blocks.push(blockchain.get_block(block_hash));
                        }
                    }
                    if new_blocks.len() > 0 {
                        peer.write(Message::Blocks(new_blocks));
                    }
                }

                Message::Blocks(blocks) => {
                    /*
                        Iterate through the blocks sent by the peer,
                        check if block hash is present in the blockchain --
                        if not, add to blockchain and a vector of new block hashes.
                        The vector of new block hashes is then sent to all peers.
                    */
                    println!("Received a Blocks message from peer");
                    let now = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_micros();

                    let mut new_block_hashes: Vec<H256> = Vec::new();
                    let mut orphan_block_hashes: Vec<H256> = Vec::new();
                    
                    let mut blockchain = self.arc_blockchain.lock().unwrap();
                    
                    for block in blocks {
                        if !blockchain.contains_block_hash(block.hash()) {
                            info!("Received a new block from peer, hash = {:?}", block.hash());

                            total_block_delay += (now - block.header.timestamp);
                            total_block_count += 1;
                            let status: InsertStatus = blockchain.insert(&block);
                            match status {
                                InsertStatus::Invalid => {}
                                InsertStatus::Orphan => {
                                    orphan_block_hashes.push(block.header.parent);
                                }
                                InsertStatus::Valid(curr_block_hashes) => {
                                    new_block_hashes.extend(curr_block_hashes);
                                }
                            }
                        }
                    }
                    info!("Statistics: Total blocks (chain + forks) = {}, Size of longest chain = {}", blockchain.get_total_blocks(), blockchain.depth());
                    let longest_chain = blockchain.all_blocks_in_longest_chain();
                    drop(blockchain);

                    // print longest chain
                    println!("Longest chain");
                    for block_hash in longest_chain {
                        print!(" {:?} ", block_hash);
                    }
                    println!("");

                    if orphan_block_hashes.len() > 0 {
                        peer.write(Message::GetBlocks(orphan_block_hashes))
                    }

                    if new_block_hashes.len() > 0 {
                        self.server.broadcast(Message::NewBlockHashes(new_block_hashes));
                    }
                }

                Message::NewTransactionHashes(trans_hashes) => {
                    /*
                        When you receive new transaction hashes, check if mempool has received them. 
                        Ignore if received before, otherwise add to `new_trans_hashes` vector 
                        and query peer for new transactions using `GetTransactions` message.
                    */
                    let mut new_trans_hashes: Vec<H256> = Vec::new();
                    let mempool = self.arc_mempool.lock().unwrap();
                    for trans_hash in trans_hashes {
                        if !mempool.has_received(trans_hash) {
                            new_trans_hashes.push(trans_hash);
                        }
                    }
                    if new_trans_hashes.len() > 0 {
                        peer.write(Message::GetTransactions(new_trans_hashes));
                    }
                }

                Message::GetTransactions(trans_hashes) => {
                    /*
                        When a peer asks for transactions, check if you have received it 
                        and not already processed it. 
                        If it is processed, it is present in some new block. 
                        Hence, do not forward processed transactions. (subject to change)
                    */
                    let mut new_trans: Vec<SignedTransaction> = Vec::new();
                    let mempool = self.arc_mempool.lock().unwrap();

                    for trans_hash in trans_hashes {
                        if mempool.has_received(trans_hash) && !mempool.has_processed(trans_hash) {
                            new_trans.push(mempool.get_transaction(trans_hash));
                        }
                    }
                    if new_trans.len() > 0 {
                        peer.write(Message::Transactions(new_trans));
                    }
                }

                Message::Transactions(signed_trans_list) => {
                    let mut mempool = self.arc_mempool.lock().unwrap();
                    let mut new_trans_hashes: Vec<H256> = Vec::new();
                    for signed_trans in signed_trans_list {
                        if (verify_signed_trans(signed_trans.clone())) {
                            if !mempool.has_received(signed_trans.hash()) {
                                println!("Received a new transaction from peer, hash = {:?}", signed_trans.hash());
                                mempool.insert(signed_trans.clone());
                                new_trans_hashes.push(signed_trans.hash());
                            }
                        } else {
                            println!("[worker] Invalid transaction: signature check failed]");
                        }
                    }
                    
                    // Broadcast the hashes of newly added transactions
                    if new_trans_hashes.len() > 0 {
                        self.server.broadcast(Message::NewTransactionHashes(new_trans_hashes));
                    }
                }

                Message::NewAddress(peer_address) => {
                    let mut directory = self.arc_directory.lock().unwrap();
                    if (directory.insert(peer_address)) {
                        println!("Received a new address {:?}", peer_address);
                        self.server.broadcast(Message::NewAddress(peer_address));
                    }
                }
            }
        }
    }
}
