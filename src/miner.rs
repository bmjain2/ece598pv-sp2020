use crate::network::server::Handle as ServerHandle;

use log::{info, warn};

use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time;
use std::time::{SystemTime, UNIX_EPOCH, Duration, Instant};

use std::thread;
use rand::Rng;
use super::block::{Block, Header, Content, get_difficulty};
use crate::crypto::hash::{Hashable, generate_random_H256};
use super::blockchain::{Blockchain, InsertStatus};
use crate::mempool::{Mempool};

use std::sync::{Arc, Mutex};
use crate::crypto::merkle::{MerkleTree};
use crate::network::message::Message;


enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    /// Channel for receiving control signal
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    server: ServerHandle,
    arc_blockchain: Arc<Mutex<Blockchain>>,
    arc_mempool: Arc<Mutex<Mempool>>,
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new(
    server: &ServerHandle,
    arc_blockchain: &Arc<Mutex<Blockchain>>, 
    arc_mempool: &Arc<Mutex<Mempool>>
) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        server: server.clone(),
        arc_blockchain: Arc::clone(arc_blockchain),
        arc_mempool: Arc::clone(arc_mempool),
    };

    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan.send(ControlSignal::Exit).unwrap();
    }

    pub fn start(&self, lambda: u64) {
        self.control_chan
            .send(ControlSignal::Start(lambda))
            .unwrap();
    }
}

impl Context {
    pub fn start(mut self) {
        thread::Builder::new()
            .name("miner".to_string())
            .spawn(move || {
                self.miner_loop();
            })
            .unwrap();
        info!("Miner initialized into paused mode");
    }

    fn handle_control_signal(&mut self, signal: ControlSignal) {
        match signal {
            ControlSignal::Exit => {
                info!("Miner shutting down");
                self.operating_state = OperatingState::ShutDown;
            }
            ControlSignal::Start(i) => {
                info!("Miner starting in continuous mode with lambda {}", i);
                self.operating_state = OperatingState::Run(i);
            }
        }
    }

    fn miner_loop(&mut self) {
        // main mining loop
        let experiment_duration = 60;   // in secs
        let start = Instant::now();
        let mut num_blocks = 0;

        loop {
            // check and react to control signals
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    self.handle_control_signal(signal);
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        self.handle_control_signal(signal);
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("Miner control channel detached"),
                },
            }
            if let OperatingState::ShutDown = self.operating_state {
                return;
            }

            if let OperatingState::Run(i) = self.operating_state {
                if i != 0 {
                    let interval = time::Duration::from_micros(i as u64);
                    thread::sleep(interval);
                }

                /*
                    Try to mine a new block by trying random nonce,
                    if you find a new block that satisifies required difficulty level --
                    insert it in the blockchain and broadcast to peers.
                */
                while (true) {

                    while (true) {
                        let mut mempool = self.arc_mempool.lock().unwrap();
                        if (mempool.is_empty()) {
                            drop(mempool);
                            // sleep for sometime before trying again
                            let sleep_interval = time::Duration::from_secs(1);
                            thread::sleep(sleep_interval);
                        } else {
                            drop(mempool);
                            break;
                        }
                    }

                    // Fetch transactions from the mempool
                    let mut mempool = self.arc_mempool.lock().unwrap();
                    let data = mempool.get_transactions(5);
                    drop(mempool);

                    let content = Content {
                        data: data.clone(),
                    };

                    let blockchain = self.arc_blockchain.lock().unwrap();
                    let parent_hash = blockchain.tip();
                    drop(blockchain);

                    let difficulty = get_difficulty();  // static difficulty
                    let merkle_tree = MerkleTree::new(&data);
                    let root = merkle_tree.root();

                    let mut rng = rand::thread_rng();

                    let header = Header {
                        parent: parent_hash,
                        nonce: rng.gen::<u32>(),
                        difficulty: difficulty,
                        timestamp: SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_micros(),
                        merkle_root: root,
                    };
                    let block = Block {
                        header: header,
                        content: content,
                    };

                    if (block.hash() <= difficulty) {
                        let mut blockchain = self.arc_blockchain.lock().unwrap();
                        let status: InsertStatus = blockchain.insert(&block);

                        if let InsertStatus::Invalid = status {
                            println!("Mined an invalid block, rejected");
                            break;
                        }
                        
                        let depth = blockchain.depth();
                        let total_blocks = blockchain.get_total_blocks();
                        let longest_chain = blockchain.all_blocks_in_longest_chain();

                        drop(blockchain);

                        num_blocks += 1;
                        info!("Mined a new block, hash = {:?}", block.hash());
                        info!("Statistics: Number of blocks mined {}, Total blocks (chain + forks) = {}, Size of longest chain = {}", num_blocks, total_blocks, depth);

                        // print longest chain
                        println!("Longest chain");
                        for block_hash in longest_chain {
                            print!(" {:?} ", block_hash);
                        }
                        println!("");

                        let new_block_hashes = vec![block.hash()];
                        self.server.broadcast(Message::NewBlockHashes(new_block_hashes));
                        break;
                    }
                }

                if (start.elapsed().as_secs() > experiment_duration) {
                    info!("[Miner] finished experiment, shutting down ...");
                    self.operating_state = OperatingState::ShutDown;
                    break;
                }
            }
            
        }
        
    }
}
