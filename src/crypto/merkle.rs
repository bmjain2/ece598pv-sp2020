use super::hash::{Hashable, H256, generate_random_H256};
use ring::digest;
use hex_literal::hex;


/// A Merkle tree.
#[derive(Debug, Default)]
pub struct MerkleTree {
    hashtree: Vec<Vec<H256>>,
    count: usize,
}

impl MerkleTree {
    pub fn new<T>(data: &[T]) -> Self where T: Hashable, {
        
        let count = data.len();
        let mut hashtree: Vec<Vec<H256>> = Vec::new();

        let mut curr: Vec<H256> = Vec::new();
        for x in data {
            curr.push(x.hash());
        }

        if curr.len() % 2 != 0 {
            let last = curr[curr.len()-1];
            curr.push(last);
        }

        let mut curr_copy = curr.clone();
        hashtree.push(curr_copy);

        let mut success = false;

        while (curr.len() > 1) {
            let mut next: Vec<H256> = Vec::new();
            let limit = curr.len() / 2;
            for i in 0 .. limit {
                let mut ctx = digest::Context::new(&digest::SHA256);
                ctx.update(&curr[2*i].as_ref());
                ctx.update(&curr[2*i + 1].as_ref());
                let combo = ctx.finish();
                next.push(combo.into());
            }
            curr = next;
            if curr.len() == 1 {
                success = true;
                break
            }
            if curr.len() % 2 != 0 {
                let last = curr[curr.len()-1];
                curr.push(last);
            }
            let mut curr_copy = curr.clone();
            hashtree.push(curr_copy);
        }
        if success {
            hashtree.push(curr);
        }

        MerkleTree {
            hashtree: hashtree,
            count: count,
        }  
    }

    pub fn root(&self) -> H256 {
        if self.count == 0 {
            // return 00..00 (has to be deterministic -- used in genesis)
            return (hex!("0000000000000000000000000000000000000000000000000000000000000000")).into(); 
        } 
        self.hashtree[self.hashtree.len() - 1][0]
    }

    /// Returns the Merkle Proof of data at index i
    pub fn proof(&self, index: usize) -> Vec<H256> {
        if !(index < self.count) {
            panic!("Invalid index {}", index)
        }

        let mut proof_data: Vec<H256> = Vec::new();
        let mut level = 0;
        let mut idx = index;
        while level < (self.hashtree.len()-1) {
            if idx % 2 == 0 {
                proof_data.push(self.hashtree[level][idx+1])
            } else {
                proof_data.push(self.hashtree[level][idx-1])
            }
            idx = idx / 2;
            level = level + 1;
        }
        proof_data
    }
}

/// Verify that the datum hash with a vector of proofs will produce the Merkle root. Also need the
/// index of datum and `leaf_size`, the total number of leaves.
pub fn verify(root: &H256, datum: &H256, proof: &[H256], index: usize, leaf_size: usize) -> bool {
    let mut rev_proof = proof.to_vec();
    let mut idx = index;
    rev_proof.reverse();
    let mut curr = *datum;
    while (!rev_proof.is_empty()) {
        let last = rev_proof.pop().unwrap();
        let mut ctx = digest::Context::new(&digest::SHA256);
        if idx % 2 == 0{
            ctx.update(&curr.as_ref());
            ctx.update(&last.as_ref());
        } else {
            ctx.update(&last.as_ref());
            ctx.update(&curr.as_ref());
        }
        idx = idx / 2;
        
        let combo = ctx.finish();
        curr = combo.into();
    }
    
    println!("{:?}, {:?}", curr, *root);
    (curr == *root)
}

#[cfg(test)]
mod tests {
    use crate::crypto::hash::H256;
    use super::*;

    macro_rules! gen_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    macro_rules! gen_big_merkle_tree_data {
        () => {{
            vec![
                (hex!("0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
                (hex!("0a0b0c0d0e0f0e0d010101010101010101010101010101010101010101010202")).into(),
                (hex!("01010101010101010101010101010101010101010101010101010c0d0e0f0e0d")).into(),
                (hex!("010101010101010101010f0e0d0a010101010101010101010101010101010202")).into(),
                (hex!("010101010101010101010101010101010f0e0d0a010101010101010101010202")).into(),
                (hex!("0101010101010101010101010101010101010101010101010101010101010202")).into(),
                (hex!("0a0b0c0d0e0f0e0d010101010101010101010101010101010101010101010202")).into(),
            ]
        }};
    }

    #[test]
    fn root() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920")).into()
        );
        // "b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0" is the hash of
        // "0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d0a0b0c0d0e0f0e0d"
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
        // "6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920" is the hash of
        // the concatenation of these two hashes "b69..." and "965..."
        // notice that the order of these two matters
    }

    #[test]
    fn root_big() {
        let input_data: Vec<H256> = gen_big_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let root = merkle_tree.root();
        assert_eq!(
            root,
            (hex!("17dc7852a503bb781cef6e7792b7b478425f7e02d06968dd5dcf8011e504e10f")).into()
        );
        // Computed using https://www.fileformat.info/tool/hash.htm
        // h(1) = b69566be6e1720872f73651d1851a0eae0060a132cf0f64a0ffaea248de6cba0
        // h(2) = 965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f
        // h(3) = 4566892b9e2517c48fc355a5bd8239f78835cc83abb2d89c372741dcebce840a
        // h(4) = 9113ac3c43b5dd5ab71eabd339078ee41354db140b15b6a97a0d283d10b099be
        // h(5) = c3b711e5f0dedf228878341ee9cb24586e94d6f37f460df42184e50de22a5be4
        // h(6) = 8ee7bfef71e0bef66ab712c7875b728e9b7637270d291657373b502159bb8550
        // h(7) = 965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f
        // h(8) = 4566892b9e2517c48fc355a5bd8239f78835cc83abb2d89c372741dcebce840a

        // h(12) = 6b787718210e0b3b608814e04e61fde06d0df794319a12162f287412df3ec920
        // h(34) = 26866b7c6e511d11ba18b470a95645b724be0d14e619739a9edcb1c9bc511dbc
        // h(56) = 7e8bc11de0096e8d65b73eeecf76d5bf626faae4db143d2e9616536c1b2e6478
        // h(78) = 7e702a7bab7791c6f0f41a4df302be7b7ac784c03cd645b219148dca1fd85901

        // h(1234) = e3c3e6b235d088745bef02dc94a88f01d07cbffbf80a4ae06cf148d496c4bc5e
        // h(5678) = 4a4682a2a62d9e79f4397e248e1a5a610195d21b538f18532d74c248391aeb4b

        // h(12345678) = 17dc7852a503bb781cef6e7792b7b478425f7e02d06968dd5dcf8011e504e10f

    }

    #[test]
    fn proof() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert_eq!(proof,
                   vec![hex!("965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f").into()]
        );
        // "965b093a75a75895a351786dd7a188515173f6928a8af8c9baa4dcff268a4f0f" is the hash of
        // "0101010101010101010101010101010101010101010101010101010101010202"
    }

    #[test]
    fn verifying() {
        let input_data: Vec<H256> = gen_merkle_tree_data!();
        let merkle_tree = MerkleTree::new(&input_data);
        let proof = merkle_tree.proof(0);
        assert!(verify(&merkle_tree.root(), &input_data[0].hash(), &proof, 0, input_data.len()));
    }

    #[test]
    fn verifying_big() {
        let mut input_data: Vec<H256> = gen_big_merkle_tree_data!();
        while input_data.len() > 0 {
            let merkle_tree = MerkleTree::new(&input_data);
            for i in 0..input_data.len(){
                let proof = merkle_tree.proof(i);
                assert!(verify(&merkle_tree.root(), &input_data[i].hash(), &proof, i, input_data.len()));
            }
            println!("All tests passed for merkle tree with {} leaves", input_data.len());
            input_data.pop();
        }        
    }
}
