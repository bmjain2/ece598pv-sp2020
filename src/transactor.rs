use crate::network::server::Handle as ServerHandle;
use crate::mempool::{Mempool};
use std::thread;
use crate::transaction::{Transaction, SignedTransaction, sign};
use crate::crypto::hash::{H256, Hashable, H160};
use std::sync::{Arc, Mutex};
use crate::network::message::Message;
use std::time;
use log::{info, warn};
use crate::crypto::key_pair;
use rand::Rng;
use std::collections::{HashSet, HashMap};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, UnparsedPublicKey, ED25519};
use rand::seq::SliceRandom;
use rand::thread_rng;
use crate::directory::{Directory};
use crate::blockchain::{Blockchain};
use crossbeam::channel::{unbounded, Receiver, Sender, TryRecvError};
use std::time::{Instant};

enum ControlSignal {
    Start(u64), // the number controls the lambda of interval between block generation
    Exit,
}

enum OperatingState {
    Paused,
    Run(u64),
    ShutDown,
}

pub struct Context {
    control_chan: Receiver<ControlSignal>,
    operating_state: OperatingState,
    server: ServerHandle,
    arc_blockchain: Arc<Mutex<Blockchain>>,
    arc_mempool: Arc<Mutex<Mempool>>,
    arc_directory: Arc<Mutex<Directory>>,
}

#[derive(Clone)]
pub struct Handle {
    /// Channel for sending signal to the miner thread
    control_chan: Sender<ControlSignal>,
}

pub fn new (
    server: &ServerHandle,
    arc_blockchain: &Arc<Mutex<Blockchain>>,
    arc_mempool: &Arc<Mutex<Mempool>>,
    arc_directory: &Arc<Mutex<Directory>>,
) -> (Context, Handle) {
    let (signal_chan_sender, signal_chan_receiver) = unbounded();

    let ctx = Context {
        control_chan: signal_chan_receiver,
        operating_state: OperatingState::Paused,
        server: server.clone(),
        arc_blockchain: Arc::clone(arc_blockchain),
        arc_mempool: Arc::clone(arc_mempool),
        arc_directory: Arc::clone(arc_directory),
    };

    let handle = Handle {
        control_chan: signal_chan_sender,
    };

    (ctx, handle)
}

impl Handle {
    pub fn exit(&self) {
        self.control_chan
            .send(ControlSignal::Exit)
            .unwrap();
    }

    pub fn start(&self, lambda: u64) {
        self.control_chan
            .send(ControlSignal::Start(lambda))
            .unwrap();
    }
}

impl Context {
    pub fn start(mut self) {        
        thread::Builder::new()
            .name("trans_generator".to_string())
            .spawn(move || {
                self.transactor_loop();
            })
            .unwrap();
        info!("Transactor initialized into paused mode");
    }

    fn handle_control_signal(&mut self, signal: ControlSignal) {
        match signal {
            ControlSignal::Exit => {
                info!("Transactor shutting down");
                self.operating_state = OperatingState::ShutDown;
            }
            ControlSignal::Start(i) => {
                info!("Miner starting in continuous mode with lamda {}", i);
                self.operating_state = OperatingState::Run(i);
            }
        }
    }

    fn transactor_loop(&mut self) {
        
        loop {
            match self.operating_state {
                OperatingState::Paused => {
                    let signal = self.control_chan.recv().unwrap();
                    self.handle_control_signal(signal);
                    continue;
                }
                OperatingState::ShutDown => {
                    return;
                }
                _ => match self.control_chan.try_recv() {
                    Ok(signal) => {
                        self.handle_control_signal(signal);
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => panic!("Miner control channel detached"),
                },
            }

            if let OperatingState::ShutDown = self.operating_state {
                return;
            }

            if let OperatingState::Run(i) = self.operating_state {

                let experiment_duration = 60;
                let start = Instant::now();

                let mut moi_address: Vec<H160> = Vec::new();
                let mut address2key: HashMap<H160, Ed25519KeyPair> = HashMap::new();
                for i in 0..2 {
                    let key = key_pair::random();
                    let pubkey = *key.public_key();
                    let pk_hash = ring::digest::digest(&ring::digest::SHA256, pubkey.as_ref());
                    let pk_hash_bytes = pk_hash.as_ref();
                    let mut raw_address: [u8; 20] = [0; 20];
                    raw_address.copy_from_slice(pk_hash_bytes.get(12..).unwrap());
                    moi_address.push(raw_address.into());
                    address2key.insert(raw_address.into(), key);
                }

                let mut directory = self.arc_directory.lock().unwrap();
                for address in &moi_address {
                    directory.insert(*address);
                    self.server.broadcast(Message::NewAddress(*address));
                }
                drop(directory);

                // sleep for some time to allow peer address processsing
                thread::sleep(time::Duration::from_secs(2));

                // Perform initial coin offering for all addresses
                let directory = self.arc_directory.lock().unwrap();
                let all_address = directory.get_all_addresses();
                drop(directory);
                let mut blockchain = self.arc_blockchain.lock().unwrap();
                blockchain.populate_ico_state(all_address);
                info!("Initial coin offering");
                println!("{:?}",  blockchain.get_longest_chain_state());
                drop(blockchain);

                loop {
                    if i != 0 {
                        let interval = time::Duration::from_micros(i as u64);
                        thread::sleep(interval);
                    }
                    // simulate transaction generation
                    let from_addr = *moi_address.choose(&mut rand::thread_rng()).unwrap();
                    let directory = self.arc_directory.lock().unwrap();
                    let to_addr = directory.get_rand_address();
                    drop(directory);
        
                    if (from_addr == to_addr) {
                        continue;
                    }
                
                    let blockchain = self.arc_blockchain.lock().unwrap();
                    let lc_state = blockchain.get_longest_chain_state();
                    drop(blockchain);
        
                    let acc_details = lc_state.get_account_data(from_addr);
                
                    // create a transaction that is valid wrt the state of the blockchain tip
                    let t = Transaction {
                        sender_addr: from_addr,
                        recv_addr: to_addr,
                        value: acc_details.balance / f64::from(10),
                        nonce: acc_details.nonce + 1,
                    };
        
                    let from_key = address2key.get(&from_addr).unwrap();
                    let signature = sign(&t, from_key);
                    let pubkey = *from_key.public_key();
                    
                    let signed_trans = SignedTransaction {
                        transaction: t.clone(),
                        sign_bytes: signature.as_ref().to_vec(),
                        pk_bytes: pubkey.as_ref().to_vec(),
                    };

                    println!("Generated a new transaction, hash = {:?}", signed_trans.hash());
                    println!("Tx hash = {:?}, from = {:?}, to = {:?}, value = {:?}", 
                        signed_trans.hash(), t.sender_addr, t.recv_addr, t.value);
        
                    let mut mempool = self.arc_mempool.lock().unwrap();        
                    mempool.insert(signed_trans.clone());
                    self.server.broadcast(Message::NewTransactionHashes(vec![signed_trans.hash()]));
                    drop(mempool);

                    if (start.elapsed().as_secs() > experiment_duration) {
                        info!("[Transactor] finished experiment, shutting down ...");
                        self.operating_state = OperatingState::ShutDown;
                        break;
                    }
                }
            }
        }
    }
}