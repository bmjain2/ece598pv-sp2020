use std::collections::{HashMap};
use crate::crypto::hash::{H160};

#[derive(Clone)]
pub struct AccountData {
    pub nonce: u32,
    pub balance: f64,
}

#[derive(Clone)]
pub struct State {
    // account address to details mapping
    state: HashMap<H160, AccountData>
}

impl State {
    pub fn new() -> Self {
        State {
            state: HashMap::new(),
        }
    }

    pub fn insert(&mut self, address: H160, details: AccountData) {
        self.state.insert(address, details);
    }

    pub fn get_account_data(&self, address: H160) -> AccountData {
        self.state.get(&address).unwrap().clone()
    }

    pub fn get_copy(&self) -> State {
        self.clone()
    }
}

impl std::fmt::Debug for State {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for (addr, acc_data) in self.state.iter() {
            write!(f, "Address: {:?} nonce: {} balance: {}\n", addr, acc_data.nonce, acc_data.balance);
        }
        Ok(())
    }
}


